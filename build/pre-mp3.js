function __ffmpegjs(__ffmpegjs_opts) {
	__ffmpegjs_opts = __ffmpegjs_opts || {};
	
	var __ffmpegjs_mountpoint = '/blob';
	var __ffmpegjs_return = [];
	var Module = {
		arguments: __ffmpegjs_opts.arguments || [],
		stdin: function() {},
		print: function(line) {
			self.postMessage({
				type: 'stdout',
				data: line
			});
		},
		printErr: function(line) {
			self.postMessage({
				type: 'stderr',
				data: line
			});
		},
		preRun: function() {
			FS.mkdir(__ffmpegjs_mountpoint);
			FS.mount(WORKERFS, {
				blobs: __ffmpegjs_opts.inputFiles || [],
			}, __ffmpegjs_mountpoint);
		},
		postRun: function() {
			FS.unmount(__ffmpegjs_mountpoint);
			
			(__ffmpegjs_opts.outputFiles || []).forEach(function(filename) {
				try {
					var obj = FS.findObject(filename);
					__ffmpegjs_return.push(obj && new Blob([new Uint8Array(obj.contents)]));
					FS.unlink(filename);
				}
				catch (e) {
					__ffmpegjs_return.push(null);
				}
			});
		},
		onExit: function(code) {
			self.postMessage({
				type: 'exit',
				data: code
			});
		}
	};
