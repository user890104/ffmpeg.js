	return __ffmpegjs_return;
}

(function() {
	var __ffmpegjs_running = false;

	self.onmessage = function(e) {
		switch (e.data.type) {
			case 'run':
				if (__ffmpegjs_running) {
					self.postMessage({
						type: 'busy'
					});
					break;
				}
				
				__ffmpegjs_running = true;
				
				self.postMessage({
					type: 'start'
				});

				var opts = {
					arguments: e.data.arguments || [],				
					inputFiles: e.data.inputFiles || [],
					outputFiles: e.data.outputFiles || []
				};
				
				var time = Date.now();
				// TODO(Kagami): Should we wrap this function into try/catch in
				// case of possible exception?
				var result = __ffmpegjs(opts);
				var totalTime = Date.now() - time;
				
				self.postMessage({
					type: 'done',
					data: result,
					time: totalTime
				});
				
				__ffmpegjs_running = false;
			break;
		}
	};
})();

self.postMessage({
	type: 'ready'
});
